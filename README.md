# PhoenixHacks Website for Spring 2020

The website was made using regular HTML, JavaScript, and CSS without any extra
libraries or frameworks.

# Deployment
The website is being deployed on a Google App Engine instance as the
`phoenixhacks-sp2020` service. Google App Engine automatically scales up and
down to 0, so hosting the website for the most part falls within the free
usage tier.

# CI/CD
There is a pipeline setup for auto-deploying the website when there are any
pushes to master. The website is deployed using a service account that has
permissions to deploy the app on Google App Engine.

# For Future Websites
For future websites, use the following procedure:

1. Fork the latest website

2. Copy over the CI/CD variables from the latest website (found in repository
settings -> CI/CD -> variables)

3. Rename the service in the app.yaml file of the newly-forked repo to 
`phoenixhacks-xxxx` where `xxxx` represents the year of the hackathon the
year that the website is being made for.

4. Go to the `gcloud-configs` repo and update the dispatch.yaml according to
the README on that repo.

5. Add a new `CNAME` entry in the website DNS settings with the host `<year>`
and value `ghs.googlehosted.com`.

6. Under the App Engine settings for the project on Google Cloud, add a new
custom domain with for the new website url i.e. `2021.phoenixhacks.com`.
The URL should be `https://console.cloud.google.com/appengine/settings/domains?project=phoenixhacks`. This allows the new subdomains to point to the website being hosted on
the Google App Engine.
