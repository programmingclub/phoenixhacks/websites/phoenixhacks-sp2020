// If document isn't ready, add onload event listener
if (document.readyState !== 'loading' ) {
    document.body.addEventListener('DOMContentLoaded', onload, false);
} else {
    // Else, call onload
    onload();
}

window.addEventListener('scroll', onscroll, false);

document.getElementById("navbar-logo").addEventListener("load", onsvgload, false);

function change_svg_fill(svg, color) {

    svg.setAttribute("fill", color);
}

function set_navbar_logo_fill(color) {
    let obj = document.getElementById("navbar-logo");
    let svgDoc = obj.getSVGDocument();

    let svg;

    if (svgDoc && svgDoc.readyState === 'complete') {
        svg = svgDoc.getElementsByTagName("svg")[0];

        change_svg_fill(svg, color);
    }
}

function get_element_index(el) {
    return Array.from(el.parentNode.children).indexOf(el);

}

function get_css_variable(name) {
    return getComputedStyle(document.documentElement).getPropertyValue(name);
}

function onscroll() {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    let navbar = document.getElementById("navbar");

    let sections = document.getElementsByClassName("section");
    let navbarItems = document.getElementsByClassName("navbar-item");

    let title = document.getElementById("navbar-title");

    [].forEach.call(navbarItems, navbarItem => {
        navbarItem.classList.remove("active");

        [].some.call(sections, section => {
            sectionTop = section.offsetTop;
            sectionBottom = sectionTop + section.offsetHeight;
            anchorHrefId = navbarItem.href.split('#')[1];

            if (section.id == anchorHrefId) {
                if (scrollTop >= sectionTop && scrollTop < sectionBottom) {
                    navbarItem.classList.add("active");
                    
                    let sectionColor;

                    if (get_element_index(navbarItem.parentNode) % 2 == 0) {
                        sectionColor = get_css_variable("--primary-color-1");
                    } else {
                        sectionColor = get_css_variable("--bg-color");
                    }

                    set_navbar_logo_fill(sectionColor);
                    title.style.color = sectionColor;
                    
                    return true;
                }
            }
        });
    });
}

function isNavbarShowing() {
    let navbar = document.getElementById("navbar");

    if (navbar.classList.contains("close")) {
        return false;
    } else {
        return true;
    }
}

function toggle_navbar() {
    if (isNavbarShowing()) {
        hide_navbar();
    } else {
        show_navbar();
    }
}

function hide_navbar() {
    let navbar = document.getElementById("navbar");

    navbar.classList.add("close");
}

function show_navbar() {
    let navbar = document.getElementById("navbar");

    if (navbar.classList.contains("close")) {
        navbar.classList.remove("close");
    }
}

function onresize() {
    if (window.innerWidth <= 750) {
        hide_navbar();
    } else {
        show_navbar();
    }
}

function onload() {
    let questions = document.getElementsByClassName("question-container");
    let modal = document.getElementById("modal");
    let navbarButton = document.getElementById("navbar-button");
    let modalClose = document.getElementById("modal-close");

    if (document.cookie.indexOf("modalShown") !== -1) {
        modal.style.display = "none";
    }

    navbarButton.addEventListener('click', toggle_navbar);
    window.addEventListener('resize', onresize);
    if (window.innerWidth <= 750) {
        hide_navbar();
    }

    setInterval(countdown, 1000);

    modalClose.addEventListener('click', close_modal);

    // Add event listener to toggle 'open class' on associted answer
    [].forEach.call(questions, questionContainer => {
        questionContainer.addEventListener('click', onfaqclick,);
    });
}

function onfaqclick(ev) {
    // Get question-container
    let target = ev.target;

    while (!target.classList.contains('question-container')) {
        target = target.parentElement;
    }

    let questionContainer = target;
    

    let answer = questionContainer.querySelector(".answer");
    let question = questionContainer.querySelector(".question");
    let icon = question.querySelector("img");

    if (answer.classList.contains("answer-open")) {
        questionContainer.classList.remove("question-container-open");
        answer.classList.remove("answer-open");
        question.classList.remove("question-open");
        icon.classList.remove("question-icon-open");
    } else {
        questionContainer.classList.add("question-container-open");
        answer.classList.add("answer-open");
        question.classList.add("question-open");
        icon.classList.add("question-icon-open");
    }
}


function padToTwo(num) {
    if (num < 10) {
        num = "0" + num;
    }
    return num;
}

function countdown() {
    const countdownDate = new Date("Jan 25, 2020 12:00:00").getTime();
    let now = new Date().getTime();

    let distance = countdownDate - now;

    let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((distance % (1000 * 60)) / 1000);

    let countdownEl = document.getElementById("countdown");
    if (distance > 0) {
        countdownEl.innerHTML = padToTwo(hours) + ":" + padToTwo(minutes) + ":" + padToTwo(seconds);
    } else {
        clearInterval(countdown);
        countdownEl.innerHTML = "";
        document.getElementById("btn-live").classList.remove("invisible");
    }
}


function onsvgload() {
    // Add transition to svg
    // TODO: FIX THIS
    let obj = document.getElementById("navbar-logo");
    let svgDoc = obj.getSVGDocument();
    let svg = svgDoc.getElementsByTagName("svg")[0];

    svg.style.transition = "0.5s";
}


function close_modal() {
    let modal = document.getElementById("modal");

    modal.style.height = "0";
    modal.style.padding = "0";

    let d = new Date();
    d.setTime(d.getTime() + 99999999999999);
    let expires = "expires=" + d.toUTCString();

    document.cookie = "modalShown=true; " + expires;
}
